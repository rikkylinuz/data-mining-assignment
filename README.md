# Data Mining 

The assignment is to extract meaning from the datasets using R.

Datasets and few meanings I extracted from the dataset:
- Earthquake dataset:   https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.csv
    1.	Which city or state had more earthquake events in the past month?
    2.	What is the minimum, maximum, average magnitude recorded on n top frequent location?
    3.	Highest magnitude earthquake recorded in the past month?
    4.	What is the relationship between depth and magnitude?
    5.	Which time or day of the week earthquake occur often?
- Movies dataset:   https://datasets.imdbws.com/
    1.	What are the popular movies in each genre in the given year? 
    2.	What are the highly rated movies given movie and genre?
    3.	Which genre movies are highly rated/liked by the users over the year and we can find the trend of the popular genre of each year in a period of time? And average running times over the period?
    4.	KNN implementation to find nearest movies based on ratings.
    5.	Kmeans implementation to find nearest movies based on ratings.



